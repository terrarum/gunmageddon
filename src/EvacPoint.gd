extends Node2D
class_name EvacPoint

onready var main = get_node('/root/Main')
onready var player = get_node('/root/Main/Player')

func _on_Area2D_body_entered(body):
	if !body:
		return
	
	if body == player:
		main.queue_free()
		var _scene = get_tree().change_scene("res://scenes/GameWon.tscn")

