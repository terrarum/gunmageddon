extends Node2D
class_name Bullet

var velocity = Vector2.ZERO
var speed = 3000
var damage = 20

onready var AudioManager = get_node("/root/Main/AudioManager")

func _ready():
	AudioManager.play_pistol_fire()

func _physics_process(delta):
	position += velocity * delta

func _on_Area2D_body_entered(body: Node2D):
	if body.is_in_group('zombie'):
		body.get_node('Health').damage(damage)
		queue_free()
	
	# Go through Survivors. Should use a mask but here we are.
	if !body.is_in_group('survivor'):
		queue_free()

func _on_Timer_timeout():
	queue_free()
