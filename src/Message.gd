extends Label

onready var player = get_node('/root/Main/Player')

var tutorial_text: = {
	'movement': 'W A S D keys to move',
	'sprint': 'Hold SHIFT to sprint',
	'arrow': 'The ARROW points to the Evac Point',
	'zombie': 'Run away from ZOMBIES',
	'survivor': 'SURVIVORS will protect you',
}

func _ready():
	set_text('movement')
	yield(get_tree().create_timer(6), "timeout")
	set_text('sprint')
	yield(get_tree().create_timer(6), "timeout")
	set_text('arrow')
	yield(get_tree().create_timer(6), "timeout")
	set_text('zombie')
	yield(get_tree().create_timer(6), "timeout")
	set_text('survivor')

func set_text(text_key: String) -> void:
	yield(get_tree().create_timer(1), "timeout")
	show()
	text = tutorial_text[text_key]
	yield(get_tree().create_timer(5), "timeout")
	hide()
