extends KinematicBody2D
class_name Zombie

onready var AudioManager = get_node("/root/Main/AudioManager")
onready var nav_2d: Navigation2D = get_node("/root/Main/World")

var attack_damage: = 20
var speed: = 250
var target_approach: Node2D
var target_attack: Node2D
var path: = []
var enemies: = []
var can_see_enemies: = false

# survivors, walls - layers the ray should collide with
var zombie_los_collision_mask = 0b00000000000000000011

func _physics_process(_delta: float) -> void:
	var visible_enemies: Array = $Ray.get_visible_targets(enemies, zombie_los_collision_mask)
	
	if visible_enemies.empty():
		can_see_enemies = false
	
	if !visible_enemies.empty() && !can_see_enemies:
		can_see_enemies = true;
		AudioManager.play_zombie_alarm()
	
	if !visible_enemies.find(target_approach):
		target_approach = null
	
	if target_approach == null:
		if !visible_enemies.empty():
			target_approach = visible_enemies[0]
			path = nav_2d.get_simple_path(global_position, target_approach.global_position, false)
			look_at(target_approach.global_position)
	
	if path.size() > 0:
		move()
		$AnimatedSprite.set_frame(1)
	else:
		target_approach = null
		$AnimatedSprite.set_frame(0)
		
func move() -> void:
	var velocity: = Vector2()

	if position.distance_to(path[0]) < 50:
		path.remove(0)
		
	if path.size() == 0:
		return

	velocity = path[0] - position
	
	velocity = velocity.normalized() * speed
	
	var _result: = move_and_slide(velocity)
	rotation = velocity.angle()

func _on_CheckRange_body_entered(body: Node2D):	
	if body.is_in_group('survivor'):
		enemies.append(body)

func _on_CheckRange_body_exited(body: Node2D):
	if !body.is_in_group('survivor'):
		return
		
	var index = enemies.find(body)
	
	if index == -1:
		return
	
	enemies.remove(index)

func _on_AttackRange_body_entered(body: Node2D):
	if body.is_in_group('survivor') && target_attack == null:
		target_attack = body
		AudioManager.play_zombie_attack()
		body.get_node('Health').damage(attack_damage)
		$AttackTimer.start()

func _on_AttackRange_body_exited(body):
	if !body:
		return
		
	if body == target_attack:
		target_attack = null
		$AttackTimer.stop()

func _on_AttackTimer_timeout():
	if target_attack == null:
		return
	target_attack.get_node('Health').damage(attack_damage)
	AudioManager.play_zombie_attack()

func _on_Health_taking_damage():
	AudioManager.play_zombie_attack()
