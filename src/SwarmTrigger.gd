extends Area2D

onready var nav_2d: Navigation2D = get_node("/root/Main/World")
onready var Player = get_node("/root/Main/Player")

var zombies: = []

func send_swarm():
	for zombie in zombies:
		if !is_instance_valid(zombie):
			continue
		
		yield(get_tree().create_timer(0.1), "timeout")
		zombie.path = nav_2d.get_simple_path(zombie.global_position, Player.global_position, false)
	
	queue_free()

func _on_SwarmTrigger_body_entered(body: KinematicBody2D):
	if !body:
		return
	
	if body.is_in_group('zombie'):
		zombies.append(body)

	if body == Player:
		send_swarm()
