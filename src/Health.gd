extends Node2D
class_name Health

signal taking_damage

onready var Main = get_node('/root/Main')
onready var Player = get_node('/root/Main/Player')
var Blood = preload('res://Blood.tscn')

export var max_health: float = 100
export var health: float = 100

func _process(_delta):
	global_rotation = 0

func heal(amount: float):
	_modify_health(amount)

func damage(amount: float):
	emit_signal("taking_damage")
	_modify_health(-amount)
	
func _modify_health(amount: float):
	health += amount
	health = clamp(health, 0, max_health)
	$HealthBar.value = health
	if health == 0:
		die()

func die():	
	# load in blood
	var blood = Blood.instance()
	
	var parent: Node2D = get_parent()
	
	blood.global_transform = parent.global_transform
	
	# remove parent
	parent.queue_free()
	
	Main.add_child(blood)
	
	if parent == Player:
		Main.queue_free()
		var _scene = get_tree().change_scene("res://scenes/GameLost.tscn")
