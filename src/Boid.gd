extends Node2D
class_name Boid

onready var parent: KinematicBody2D = get_parent()
onready var player: KinematicBody2D = get_node('/root/Main/Player')

var boids: = []
var max_move_speed: int = 220
var move_speed: int = 220
var sprint_multiplier: int = 2
var perception_radius: int = 200
var velocity: = Vector2()
var acceleration: = Vector2()
var steer_force: float = 20.0
var alignment_force: float = 0.3
var cohesion_force: float = 0.2
var seperation_force: float = 1.0

func _ready():
	velocity = Vector2(1, 1).normalized() * move_speed

func _physics_process(_delta):
	return
	if !parent.is_in_group('follower'):
		return
	
	var neighbours: = get_tree().get_nodes_in_group('follower')
	var index: = neighbours.find(parent)
	neighbours.remove(index)
	neighbours.append(player)
	
	if neighbours.empty():
		return
	
	# Lower speed when close to player.
	var distance_to_player = global_position.distance_to(player.global_position)
	if distance_to_player < perception_radius:
		move_speed = max_move_speed * (distance_to_player / max_move_speed) - 100
		move_speed = move_speed if move_speed > 60 else 0
		move_speed = clamp(move_speed, 0, max_move_speed)
	else:
		move_speed = max_move_speed
		
	if Input.is_action_pressed("sprint"):
		move_speed *= sprint_multiplier
	
	acceleration += process_alignments(neighbours) * alignment_force
	acceleration += process_cohesion(neighbours) * cohesion_force
	acceleration += process_seperation(neighbours) * seperation_force
	
	velocity += acceleration
	velocity = velocity.clamped(move_speed)
	
	velocity = parent.move_and_slide(velocity)

func process_alignments(neighbours: Array) -> Vector2:
	var vector: = Vector2()
	if neighbours.empty():
		return vector
	
	for boid in neighbours:
		if boid == player:
			vector += player.velocity
		else:
			vector += boid.get_node('Boid').velocity
	
	vector /= neighbours.size()
	
	return steer(vector.normalized() * move_speed)

func process_cohesion(neighbours: Array) -> Vector2:
	var vector: = Vector2()
	if neighbours.empty():
		return vector
	
	for boid in neighbours:
		vector += boid.global_position
	
	vector /= neighbours.size()
	
	return steer((vector - global_position).normalized() * move_speed)
	
func process_seperation(neighbours: Array) -> Vector2:
	var vector: = Vector2()
	var close_neighbours: = []
	
	for boid in neighbours:
		if global_position.distance_to(boid.global_position) < perception_radius / 2:
			close_neighbours.push_back(boid)
	
	if close_neighbours.empty():
		return vector

	for boid in close_neighbours:
		var difference: Vector2 = global_position - boid.global_position
		vector += difference.normalized() / difference.length()

	vector /= close_neighbours.size()

	return steer(vector.normalized() * move_speed)

func steer(target: Vector2) -> Vector2:
	var steer: Vector2 = target - velocity
	return steer.normalized() * steer_force
