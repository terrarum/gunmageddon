extends Node2D

var Game: PackedScene = preload('res://scenes/Game.tscn')
var Zombie = preload('res://Zombie.tscn')

var vertical_offset: = 100
var spawner_position: Vector2 = Vector2()
var zombie_count = 2

func _ready():
	spawner_position = $ZombieSpawner.global_position
	
func _process(_delta):
	if Input.is_action_pressed("start_game"):
		var _scene: = get_tree().change_scene_to(Game)
		queue_free()

func get_random_survivor_position() -> Vector2:
	var survivors = get_tree().get_nodes_in_group('survivor')
	if survivors.empty():
		return Vector2(-200, 500)
	
	var survivor = survivors[randi() % survivors.size()]
	return survivor.global_position
	
func _on_SpawnTimer_timeout():
	for n in zombie_count:
		var zombie = Zombie.instance()
		zombie.global_position = Vector2(
			rand_range(spawner_position.x - 50, spawner_position.x + 50),
			rand_range(spawner_position.y - 150, spawner_position.y + 150)
		)
		add_child(zombie)
		var path = $World.get_simple_path(zombie.global_position, get_random_survivor_position(), false)
		zombie.path = path
