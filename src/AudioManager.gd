extends Node
class_name AudioManager

var pistol_fire: = []
var zombie_alarm: = []
var zombie_attack: = []

func _ready():
	pistol_fire = [$PistolFireA, $PistolFireB, $PistolFireC]
	zombie_alarm = [$ZombieAlarmA, $ZombieAlarmB, $ZombieAlarmC, $ZombieAlarmD]
	zombie_attack = [$ZombieAttackA, $ZombieAttackB, $ZombieAttackC, $ZombieAttackD]

func play_pistol_fire():
	randomize()
	var i = randi() % pistol_fire.size()
	print(i)
	pistol_fire[i].play()
	
func play_zombie_alarm():
	zombie_alarm[randi() % zombie_alarm.size()].play()
	
func play_zombie_attack():
	zombie_attack[randi() % zombie_attack.size()].play()
