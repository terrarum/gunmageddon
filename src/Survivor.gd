extends KinematicBody2D
class_name Survivor

onready var Main = get_node('/root/Main')
onready var nav_2d: Navigation2D = get_node("/root/Main/World")
onready var Player: KinematicBody2D = get_node("/root/Main/Player")

var Bullet = preload('res://Bullet.tscn')

var enemies: = []
var can_shoot: = true
var player_approach_range = 150
var speed: = 200;
var sprint_multiplier: = 2

# zombies, walls - layers the ray should collide with
var survivor_los_collision_mask = 0b00000000000000000101

func _physics_process(_delta):
	process_movement()
	process_enemies()

func process_movement():
	if !is_in_group('follower'):
		return

	var distance_to_player = INF
	var can_see_player = $Ray.can_see(Player)

	if can_see_player:
		distance_to_player = $Ray.distance_to(Player)
		
	if !can_see_player || distance_to_player > player_approach_range:
		move()

func move() -> void:
	var velocity: = Vector2()
	var path = nav_2d.get_simple_path(global_position, Player.global_position, false)

	if position.distance_to(path[0]) < 50: # 10 is arbitrary, should really be "distance about to be moved"
		path.remove(0)
		
	if path.size() == 0:
		return

	velocity = path[0] - position
	
	var move_speed = speed
	if Input.is_action_pressed("sprint"):
		move_speed *= sprint_multiplier
	
	velocity = velocity.normalized() * move_speed
	
	var _result: = move_and_slide(velocity)
	rotation = velocity.angle()

func process_enemies():
	var visible_enemies = $Ray.get_visible_targets(enemies, survivor_los_collision_mask)

	if !visible_enemies.empty() && !Input.is_action_pressed("sprint"):
		shoot(visible_enemies[0])

func draw_weapon():
	$SpriteStanding.hide()
	$SpritePistol.show()

func holster_weapon():
	$SpritePistol.hide()
	$SpriteStanding.show()

# TODO should not be the Survivor's problem
func fire_bullet():
	var bullet: Node2D = Bullet.instance()
	bullet.position = $Muzzle.global_transform.origin
	bullet.velocity = $Muzzle.global_transform.x * bullet.speed
	bullet.rotation = bullet.velocity.angle()
	Main.add_child(bullet)

func shoot(target):
	look_at(target.global_position)
	$Muzzle.look_at(target.global_position)

	if !can_shoot:
		return

	fire_bullet()
	can_shoot = false
	yield(get_tree().create_timer(0.5), "timeout")
	can_shoot = true

func _on_CheckRange_body_entered(body: Node2D):
	if !body:
		return
		
	if body.is_in_group('zombie'):
		enemies.append(body)
		draw_weapon()

func _on_CheckRange_body_exited(body: Node2D):		
	if !body.is_in_group('zombie'):
		return

	enemies.erase(body)
	
	if enemies.empty():
		holster_weapon()
