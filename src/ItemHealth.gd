extends Node2D

onready var Player = get_node("/root/Main/Player")

var health_amount: int = 50

func _on_Area2D_body_entered(body):
	if !body:
		return
		
	if body != Player:
		return

	var survivors = get_tree().get_nodes_in_group('follower')
	survivors.append(Player)

	var heal_count = 0

	for survivor in survivors:
		var survivor_health = survivor.get_node('Health')
		if survivor_health.health < survivor_health.max_health:
			survivor_health.heal(health_amount)
			heal_count += 1
			
	if heal_count > 0:
		queue_free()
