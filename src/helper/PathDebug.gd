extends Node

onready var Main = get_node('/root/Main')

var line: = Line2D.new()
var parent

func _ready():
	line.set_as_toplevel(true)
	line.z_index = 150
	get_tree().get_root().call_deferred("add_child", line)
	parent = get_parent()

func _process(_delta):
	line.points = parent.path

func _exit_tree():
	line.queue_free()
