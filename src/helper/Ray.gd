extends Node2D
class_name RayHelper

func fire_ray(target, collision_mask = null) -> Object:
	var space_state = get_world_2d().direct_space_state
	if collision_mask == null:
		return space_state.intersect_ray(global_position, target.global_position, [get_parent()])
	else:
		return space_state.intersect_ray(global_position, target.global_position, [get_parent()], collision_mask)

func distance_to(target, collision_mask = null) -> float:
	var result = fire_ray(target, collision_mask)
	
	if result.empty():
		return INF
	
	if result.collider == target:
		return global_position.distance_to(result.position)
		
	return INF

func can_see(target, collision_mask = null) -> bool:
	var result = fire_ray(target, collision_mask)

	if result.empty():
		return false

	if result.collider == target:
		return true

	return false

func get_visible_targets(targets: Array, collision_mask = null) -> Array:
	var visible_targets: = []
	
	if targets.empty():
		return visible_targets
	
	for target in targets:
		if can_see(target, collision_mask):
			visible_targets.append(target)
	
	return visible_targets
