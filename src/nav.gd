extends TileMap

onready var ground: TileMap = $"../ground"
onready var obstacles: TileMap = $"../obstacles"
onready var fences: TileMap = $"../fences"

func _ready():	
	var ground_cells = ground.get_used_cells()
	var obstacle_cells = obstacles.get_used_cells()
	var fence_cells = fences.get_used_cells()
	
	for cell in obstacle_cells:
		ground_cells.erase(cell)
		
	for cell in fence_cells:
		ground_cells.erase(cell)
		
	for cell in ground_cells:
		set_cellv(cell, 0)
	
