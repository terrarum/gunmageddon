extends KinematicBody2D
class_name Player

var speed = 200
var sprint_multiplier = 2
var current_speed

var velocity = Vector2()
var last_angle
var current_angle

var nearby_survivors = [];
var followers = [];

func _ready():
	set_standing()

func _physics_process(_delta):
	if nearby_survivors.size() > 0:
		add_followers()
		
	set_velocity()
	set_angle()	
	velocity = move_and_slide(velocity)
	
func set_walking():
	$Walk.show()
	$Walk.play()
	$Stand.hide()
	
func set_standing():
	$Walk.hide()
	$Walk.stop()
	$Stand.show()
	
func set_velocity():
	velocity = Vector2()
	
	if Input.is_action_pressed("move_right"):
		velocity.x += 1
	if Input.is_action_pressed("move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("move_down"):
		velocity.y += 1
	if Input.is_action_pressed("move_up"):
		velocity.y -= 1
	
	current_speed = speed
	
	if Input.is_action_pressed("sprint"):
		current_speed *= sprint_multiplier
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * current_speed
		set_walking()
	else:
		set_standing()
		

func set_angle():
	current_angle = velocity.angle()
	
	if current_angle != last_angle && velocity != Vector2():
		last_angle = current_angle
		rotation = velocity.angle()

func add_followers():
	# If LoS on a nearby Survivor, mark them as a Follower.
	for survivor in nearby_survivors:
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, survivor.global_position, [self])
		if !result:
			continue
			
		if result.collider == survivor:
			survivor.add_to_group('follower')
			followers.append(survivor)
	
	# Remove from Nearby Survivors.
	for follower in followers:
		if nearby_survivors.has(follower):
			nearby_survivors.erase(follower)
#		var index = nearby_survivors.find(follower)
#		if index == -1:
#			continue
#		nearby_survivors.remove(index)

func _on_VisionRange_body_entered(body: Node2D):
	# If a Survivor is nearby mark them as a Follower candidate.
	if body.is_in_group('survivor') && !body.is_in_group('follower'):
		nearby_survivors.append(body)

func _on_VisionRange_body_exited(body: Node2D):
	# If a Survivor is no longer nearby and not a follower, remove them as a Follower candidate.
	if body.is_in_group('survivor') && !body.is_in_group('follower'):
		var index = nearby_survivors.find(body)
		if index == -1:
			return
		nearby_survivors.remove(index)
