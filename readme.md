# GUNMAGEDDON

A game made in Godot.

There are Zombies. There are Survivors. The Player is a Survivor.

Zombies approach and attack Survivors. Survivors shoot Zombies. NPC Survivors will follow the Player.

## To Do

### MVP

- [x] Give Survivors and Zombies healthbars
- [x] Zombies should approach nearby Survivors
- [x] Zombies should attack Survivors when in melee range
- [x] Survivors should shoot nearby Zombies
  - [x] They should only shoot when they have line-of-sight
- [x] Zombies and Survivors should switch to nearest visible target - ray to all enemies, sort by closest
  - even better: stick with one target until lose LoS, but switch to enemy taking damage from
- [x] Survivors should follow the Player
- [x] Survivor Boid implementation should be slightly less weird
- [x] Survivors should not shoot when sprinting
- [x] Win by reaching an evac point
- [x] Lose by dying
- [x] Menu
- [x] Instructions

### Bugs
- [x] Survivors shooting at Zombie on other side of wall
- [x] Zombies not attacking visible Survivor, possibly latched on to Survivor out of LoS
- [ ] Survivor follow behaviour is really weird, I'm not convinced flocking is the answer

### Issues
- [x] Learn how to place props on top of walkable tiles and make them be not walkable - [Solved](https://www.reddit.com/r/godot/comments/n3vfkt/how_do_i_stack_tilemaps_and_still_have/)

### Game

- [ ] Randomise Zombie/Survivor sprites when instanced
- [ ] Random Zombie movement when idle
- [x] Audio
- [x] Bigger better map
- [x] Health packs - heal Player first then distribute to Survivors
- [ ] Randomise bullet spread
- [ ] Survivors gain XP and level up health, accuracy and damage
- [ ] Further improve follower behaviour - less bouncing

## Vague Ideas

- [ ] Some kind of cover tile for Survivors to position at

## Attribution

- World/Character Assets: [Kenney's Topdown Shooter](https://kenney.nl/assets/topdown-shooter)
- Icons: [Kenney's Game Icons](https://www.kenney.nl/assets/game-icons)
- Blood: [PWL](https://opengameart.org/content/blood-splats)
- Boids implementation: https://github.com/codatproduction/Boids-simulation (ended up removing this because boids didn't solve my problem)
- Gun sounds: https://www.fesliyanstudios.com/royalty-free-sound-effects-download/gun-shooting-300
- Zombie sounds: https://www.fesliyanstudios.com/royalty-free-sound-effects-download/zombie-174
- Player walking animation and fence: https://twitter.com/pinktrashcat

